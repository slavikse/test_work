import angular from 'angular';

angular.module('app', [])
.controller('SearchController', ['$rootScope', '$scope', ($rootScope, $scope) => {
  $scope.change = () => {
    $rootScope.search = $scope.search;
  };
}]);
