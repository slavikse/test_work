import angular from 'angular';

angular.module('app')
.controller('ItemListController', ['$rootScope', '$scope', '$http', ($rootScope, $scope, $http) => {
  $http.get('data.json')
  .then(res => {
    $scope.itemList = res.data;
  });

  $scope.choice = false;
}])
.directive('choice', () => {
  return {
    restrict: 'A',
    template: `
      <button class='content__choice'
              ng-click='choice = !choice'>
        {{ choice ? 'Выбрано' : 'Выбрать' }}
      </button>
    `
  }
});
